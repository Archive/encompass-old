#include "config.h"
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <libxml/uri.h>

typedef struct {
  gchar *icon;
  gchar *url;
} mybutt;

static GnomeUIInfo bm_context_menu[] = {
  {
    GNOME_APP_UI_ITEM, N_("_Remove Bookmark"),
    N_("Remove this item from your bookmarks"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_TRASH,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("_Properties..."),
    N_("Edit this bookmarks properties"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PROP,
    0, 0, NULL
  },  
  GNOMEUIINFO_END
};

/* xml* functions copied from libxml 2 by Daniel Veillard 
   and editted by Iain Holmes so they don't use libxml specific stuff */
/* Should we escape ' ' as well? */
#define IS_RESERVED(x) (((x) == ':') || ((x) == '/') || ((x) == '?') || \
        ((x) == ':') || ((x) == '@') || ((x) == '&') || ((x) == '=') || \
        ((x) == '+') || ((x) == '$') || ((x) == ','))

static void bookmark_save_cb(GtkWidget *widget, int page, gpointer data) {
  GnomeDesktopEntry *dentry;
  char *name, *path;
  
  if (page != -1)
    return;
  
  g_return_if_fail(data != NULL);
  g_return_if_fail(GNOME_IS_DENTRY_EDIT(data));
  
  dentry = gnome_dentry_get_dentry(GNOME_DENTRY_EDIT(data));
  
  if (dentry->location)
    g_free (dentry->location);
  
  name = xmlURIEscape (dentry->name);
  path = gtk_object_get_data (data, "location");
  dentry->location = g_strconcat(path, "/", name, ".desktop", NULL);
  g_print ("location: %s\n", dentry->location);
  g_free (name);

  gnome_desktop_entry_save(dentry);
  gnome_desktop_entry_free(dentry);
}

void edit_bookmark_entry(GtkWidget *widget, gchar *file) {
  GtkWidget *dialog;
  GtkNotebookPage *page;
  GtkObject *o;
  GnomeDesktopEntry *dentry = NULL;
  GList *types = NULL;
  gchar *url;
  
  if(file && strlen(file)) {
    dentry = gnome_desktop_entry_load(file);
    
    /* We'll screw up a KDE menu entry if we edit it */
    if (dentry && dentry->is_kde) {
      gnome_desktop_entry_free (dentry);
      g_free (file);
      return;
    }
  }
  
  dialog = gnome_property_box_new();
  gtk_window_set_title(GTK_WINDOW(dialog), N_("Bookmark Properties"));
  gtk_window_set_policy(GTK_WINDOW(dialog), FALSE, FALSE, TRUE);
  
  o = gnome_dentry_edit_new_notebook(GTK_NOTEBOOK(GNOME_PROPERTY_BOX(dialog)->notebook));

  /* Yup, doing nasty evil accessing Private function stuff.
     But the fields shouldn't be changed till GNOME-Libs 2, 
     at which point it can be fixed :) */
  gtk_widget_hide (GNOME_DENTRY_EDIT (o)->terminal_button);
  gtk_widget_set_sensitive (GNOME_DENTRY_EDIT (o)->type_combo, FALSE);

  /* Get the 1st page and change the label */
  page = g_list_nth_data (GTK_NOTEBOOK (GNOME_PROPERTY_BOX (dialog)->notebook)->children, 0);

  /* Change label :) */
  gtk_label_set_text (GTK_LABEL (page->tab_label), _("Bookmark"));
  /* FIXME!!! Find out what page->menu_label is and change it to read 
     _("Bookmark") too */

  /* Hide the second page */
  gtk_widget_hide (gtk_notebook_get_nth_page (GTK_NOTEBOOK (GNOME_PROPERTY_BOX (dialog)->notebook), 1));
  
  types = g_list_append(types, "URL");
  gtk_combo_set_popdown_strings(GTK_COMBO(GNOME_DENTRY_EDIT(o)->type_combo), types);
  g_list_free(types);
  
  if (dentry) {
    gnome_dentry_edit_set_dentry(GNOME_DENTRY_EDIT(o), dentry);
    gtk_object_set_data_full (o, "location", g_strdup (file), g_free);
    gnome_desktop_entry_free(dentry);
    g_free(file);
  } else {
    file = gnome_util_home_file("apps");
    dentry = g_new0(GnomeDesktopEntry, 1);
    dentry->type = g_strdup("URL");
    
    url = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (entry)->entry));
    if (url && *url != '\0') {
      dentry->exec = g_strsplit(url, " ", 1);
      dentry->exec_length = 1;
    } else {
      dentry->exec = NULL;
      dentry->exec_length = 0;
    }
    
    dentry->name = g_strdup(gtk_html_get_title(GTK_HTML(html)));
    /*we don't have to free dirfile here it will be freed as if
      we had strduped it here*/
    gtk_object_set_data_full (o, "location", g_strdup (file), g_free);

    gnome_dentry_edit_set_dentry(GNOME_DENTRY_EDIT(o), dentry);
    gnome_desktop_entry_free(dentry);
    gnome_property_box_changed(GNOME_PROPERTY_BOX(dialog));
    
    g_free(file);
  }
    
  gtk_signal_connect_object(o, "changed",
			    GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(dialog));
  
  gtk_signal_connect(GTK_OBJECT(dialog), "apply", 
		     GTK_SIGNAL_FUNC(bookmark_save_cb), o);
  gtk_signal_connect(GTK_OBJECT(o), "destroy", 
		     GTK_SIGNAL_FUNC(gtk_object_unref), NULL);
  gtk_widget_show(dialog);
}

static void bookmark_activate_cb(GtkWidget *widget,
				 mybutt * butt) {
  if(butt->icon) {
    gnome_window_icon_set_from_file(GTK_WINDOW(window), butt->icon);
  } else {
    gnome_window_icon_set_from_file(GTK_WINDOW(window),
				    gnome_pixmap_file("encompass.png"));
  }

  goto_url(butt->url, 0);
}

static void bookmark_remove_cb(GtkWidget *widget, gchar *bookmark) {
  GnomeDesktopEntry *dentry;
  
  dentry = gnome_desktop_entry_load(bookmark);
  gnome_desktop_entry_destroy(dentry);
}

static void parse_bookmark_entry(gchar *file, GtkMenu *menu) {
  GnomeDesktopEntry *gde;
  GtkWidget *menuitem, *label, *pixmap, *bm_popup;
  mybutt * butt;
  
  gde = gnome_desktop_entry_load(file);
  butt = g_new0(mybutt, 1);

  g_return_if_fail(gde != NULL);

  if(g_strcasecmp(gde->type, "URL")) {
  }
  else {
    menuitem = gtk_pixmap_menu_item_new();
    gtk_tooltips_set_tip(tooltips, menuitem, gde->comment, NULL);
    bm_popup = gnome_popup_menu_new(bm_context_menu);
    gnome_popup_menu_attach(bm_popup, menuitem, NULL);
    gtk_widget_show(bm_popup);
    gtk_signal_connect(GTK_OBJECT(bm_context_menu[0].widget), "activate",
		       GTK_SIGNAL_FUNC(bookmark_remove_cb), gde->location);
    gtk_signal_connect(GTK_OBJECT(bm_context_menu[1].widget), "activate",
		       GTK_SIGNAL_FUNC(edit_bookmark_entry), gde->location);
    label = gtk_label_new(gde->name);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_widget_show(label);
    if(gde->icon) {
      pixmap = gnome_pixmap_new_from_file_at_size(gde->icon, 18, 18);
    } else {
      pixmap = gnome_pixmap_new_from_file_at_size(gnome_pixmap_file("encompass/url.png"), 18, 18);
    }
    gtk_pixmap_menu_item_set_pixmap(GTK_PIXMAP_MENU_ITEM(menuitem), pixmap);
    gtk_widget_show(pixmap);
    gtk_container_add(GTK_CONTAINER(menuitem), label);
    butt->icon = g_strdup(gde->icon);
    butt->url = g_strjoinv("", gde->exec);
    gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			GTK_SIGNAL_FUNC(bookmark_activate_cb), butt);
    gtk_widget_show(menuitem);
    gtk_menu_append(menu, menuitem);
  }
  g_free(gde);
}

static void parse_bookmarks_subdir(gchar *path, GtkMenu *menu) {
  GnomeDesktopEntry *gde;
  GtkWidget *menuitem, *label, *pixmap, *subdirmenu;
  gchar *file;
  
  file = g_strconcat(path, "/.directory", NULL);
  if(!g_file_test(file, G_FILE_TEST_ISFILE)) {
  }
  else {
    gde = gnome_desktop_entry_load(file);
    if(g_strcasecmp(gde->type, "Directory")) {
    }
    else {
      subdirmenu = gtk_menu_new();
      menuitem = gtk_pixmap_menu_item_new();
      label = gtk_label_new(gde->name);
      gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
      gtk_widget_show(label);
      if(gde->icon) {
	pixmap = gnome_pixmap_new_from_file_at_size(gde->icon, 18, 18);
      } else {
	pixmap = gnome_pixmap_new_from_file_at_size(gnome_pixmap_file("encompass/bookmarks.png"),
						    18, 18);
      }
      gtk_pixmap_menu_item_set_pixmap(GTK_PIXMAP_MENU_ITEM(menuitem), pixmap);
      gtk_widget_show(pixmap);
      gtk_container_add(GTK_CONTAINER(menuitem), label);
      gtk_widget_show(menuitem);
      gtk_menu_append(menu, menuitem);
      gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), subdirmenu);
      gtk_widget_show(subdirmenu);
      parse_bookmarks_dir(path, GTK_MENU(subdirmenu));
    }
    g_free(gde);
  }
  g_free(file);
}

void parse_bookmarks_dir(gchar *path, GtkMenu *menu) {
  DIR *dirp;
  struct dirent *dent;
  struct stat statbuf;
  gchar *file;
  
  if ((dirp = opendir(path)) != NULL) {
    while ((dent = readdir(dirp)) != NULL) {
      if(strcmp(dent->d_name,".") && strcmp(dent->d_name,"..")) {
        file = g_strconcat(path, "/", dent->d_name, NULL);
        if(!lstat(file,&statbuf)) {
          if(S_ISREG(statbuf.st_mode)) {
            if(!strstr(file, ".order")) {
              parse_bookmark_entry(file, menu);
            }
          }
          else if(S_ISDIR(statbuf.st_mode)) {
            parse_bookmarks_subdir(file, menu);
          }
        }
        g_free(file);
      }
    }
    closedir(dirp);
  }
}
