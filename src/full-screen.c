#include "config.h"

void full_screen_cb(GtkWidget *widget, gpointer data) {

  eprefs.FullScreenMode = GTK_CHECK_MENU_ITEM(widget)->active;
  set_fullscreen_tb_mode(eprefs.FullScreenMode);
  if(eprefs.FullScreenMode) {
	gtk_window_set_default_size (GTK_WINDOW (window),
				     gdk_screen_width (),
				     gdk_screen_height ());
	gtk_widget_set_uposition (GTK_WIDGET (window), 0, 0);
	gtk_widget_set_usize (GTK_WIDGET (window),
			      gdk_screen_width (),
			      gdk_screen_height ());
	gtk_window_set_policy (GTK_WINDOW (window),
			       FALSE, FALSE, FALSE);
	gtk_widget_hide(window);
	gtk_widget_unrealize(window);
	gtk_window_set_wmclass (GTK_WINDOW (window), "desktop_window", "Encompass");
	gtk_widget_set_events (window, gtk_widget_get_events (window) 
			      | GDK_KEY_PRESS_MASK | GDK_KEY_PRESS_MASK);
	gtk_widget_realize(window);
	gtk_widget_show(window);
	gtk_widget_hide(bar);
	gtk_widget_unrealize(bar);
	gnome_win_hints_set_layer (window, WIN_LAYER_ABOVE_DOCK);
	gnome_win_hints_set_state (window,
				   WIN_STATE_STICKY
				   | WIN_STATE_MAXIMIZED_VERT
				   | WIN_STATE_MAXIMIZED_HORIZ
				   | WIN_STATE_FIXED_POSITION
				   | WIN_STATE_HIDDEN
				   | WIN_STATE_ARRANGE_IGNORE);
	gnome_win_hints_set_hints (window,
				   WIN_HINTS_SKIP_WINLIST
				   | WIN_HINTS_SKIP_TASKBAR);
	gdk_window_move_resize (window->window,
				0, 0,
				gdk_screen_width (),
				gdk_screen_height ());
        gdk_window_set_decorations (window->window, 0);
        gdk_window_set_functions (window->window, 0);
  } else {
  	gtk_widget_realize(window);
	gtk_window_set_default_size (GTK_WINDOW (window), 320, 240);
	gtk_widget_set_uposition (GTK_WIDGET (window), 30, 30);
	gtk_widget_set_usize (GTK_WIDGET (window), 640, 480);
	gtk_window_set_policy (GTK_WINDOW (window),
			       FALSE, FALSE, FALSE);
	gtk_widget_hide(window);
	gtk_widget_unrealize(window);
	gtk_window_set_wmclass (GTK_WINDOW (window), "desktop_window", "Encompass");
	gtk_widget_realize(window);
	gtk_widget_show(window);
	gtk_widget_realize(bar);
	gtk_widget_show(bar);
	gnome_win_hints_set_layer (window, WIN_LAYER_NORMAL);
	gnome_win_hints_set_state (window, 0);
	gnome_win_hints_set_hints (window, 0);
	gdk_window_move_resize (window->window, 30, 30, 640, 480);
        gdk_window_set_decorations (window->window, 1);
        gdk_window_set_functions (window->window, 1);
  }
}
