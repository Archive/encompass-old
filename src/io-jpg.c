/* io-jpg.c
 * Copyright (C) 2000 Helix Code, Inc.
 * Author: Iain Holmes  <iain@helixcode.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/* This file is heavily based upon the "jpeg.c" file in GIMP 1.2
   which in turn is heavily based upon the "example.c" file in libjpeg.
*/
#include <gnome.h>
#include <stdio.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <jpeglib.h>
#include <jerror.h>

#include "io-jpg.h"

#define DEFAULT_QUALITY 0.75
#define DEFAULT_SMOOTHING 0.0
#define DEFAULT_OPTIMIZE 1
#define DEFAULT_PROGRESSIVE 0
#define DEFAULT_BASELINE 1
#define DEFAULT_SUBSMP 0
#define DEFAULT_RESTART 0
#define DEFAULT_DCT 0

typedef struct my_error_mgr {
  struct jpeg_error_mgr pub;

  jmp_buf setjmp_buffer;
} *my_error_ptr;

static void
my_error_exit (j_common_ptr cinfo)
{
  my_error_ptr myerr = (my_error_ptr) cinfo->err;

  (*cinfo->err->output_message) (cinfo);
  longjmp (myerr->setjmp_buffer, 1);
}

gboolean
save_jpeg (GdkPixbuf *pixbuf,
	  const char *filename)
{
  FILE *handle = NULL;
  int width, height, depth, rowstride;
  gboolean has_alpha;
  guchar *pixels;
  guchar *temp, *s, *t;
  struct jpeg_compress_struct cinfo;
  struct my_error_mgr jerr;
  int i, j;

  cinfo.err = jpeg_std_error (&jerr.pub);
  jerr.pub.error_exit = my_error_exit;

  if ((handle = fopen (filename, "wb")) == NULL) {
    return FALSE;
  }

  if (setjmp (jerr.setjmp_buffer)) {
    /* Error handler */
    jpeg_destroy_compress (&cinfo);
    if (handle)
      fclose (handle);
    return FALSE;
  }

  jpeg_create_compress (&cinfo);
  jpeg_stdio_dest (&cinfo, handle);

  width = gdk_pixbuf_get_width (pixbuf);
  height = gdk_pixbuf_get_height (pixbuf);
  depth = gdk_pixbuf_get_bits_per_sample (pixbuf);
  has_alpha = gdk_pixbuf_get_has_alpha (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  pixels = gdk_pixbuf_get_pixels (pixbuf);

  cinfo.input_components = 3;
  cinfo.image_width = width;
  cinfo.image_height = height;
  cinfo.in_color_space = JCS_RGB;
  jpeg_set_defaults (&cinfo);

  jpeg_set_quality (&cinfo, (gint) (DEFAULT_QUALITY * 100), DEFAULT_BASELINE);
  cinfo.smoothing_factor = (gint) (0.01 * 100);
  cinfo.optimize_coding = DEFAULT_OPTIMIZE;

  cinfo.comp_info[0].h_samp_factor = 2;
  cinfo.comp_info[0].v_samp_factor = 2;
  cinfo.comp_info[1].h_samp_factor = 1;
  cinfo.comp_info[1].v_samp_factor = 1;
  cinfo.comp_info[2].h_samp_factor = 1;
  cinfo.comp_info[2].v_samp_factor = 1;

  cinfo.restart_interval = 0;
  cinfo.restart_in_rows = DEFAULT_RESTART;

  cinfo.dct_method = JDCT_ISLOW;

  jpeg_start_compress (&cinfo, TRUE);
  jpeg_write_marker (&cinfo, JPEG_COM, "Created with Encompass", 22);

  temp = g_new (guchar, cinfo.image_width * cinfo.input_components);
  while (cinfo.next_scanline < cinfo.image_height) {
    t = temp;
    s = pixels;
    i = cinfo.image_width;

    while (i--) {
      for (j = 0; j < cinfo.input_components; j++) 
	*t++ = *s++;
      if (has_alpha) 
	s++;
    }

    pixels += rowstride;
    jpeg_write_scanlines (&cinfo, (JSAMPARRAY) &temp, 1);
  }

  jpeg_finish_compress (&cinfo);
  fclose (handle);
  jpeg_destroy_compress (&cinfo);
  g_free (temp);

  return TRUE;
}
    
