#ifndef _MENU_H_
#define _MENU_H_

void cancelbt_cb(GtkWidget *widget, gpointer data);
void file_quit_callback (GtkWidget *widget, gpointer data);
void create_menu(GtkWidget *app);
void set_back_menu(gboolean sense);
void set_fore_menu(gboolean sense);
void go_menu_append(GtkWidget *widget);
void parse_books(void);
GtkWidget *get_bm_menu(void);

#endif
