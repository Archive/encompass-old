#include "config.h"
#include <gtkhtml/htmlobject.h>
#include <gtkhtml/htmlimage.h>
#include <gtkhtml/htmllinktext.h>
#include <gtkhtml/htmlselection.h>
#include <gtkhtml/htmlselect.h>
#include "io.h"
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>

#define lock_gtk \
g_mutex_lock(gtk_mtx); \
gdk_threads_enter();

#define unlock_gtk \
gdk_threads_leave(); \
g_mutex_unlock(gtk_mtx);

#define process_gtk_events \
lock_gtk; \
while(gtk_events_pending()) { \
  gtk_main_iteration(); \
} \
unlock_gtk;

static gchar *parse_href (const gchar *s)
{
	gchar *retval;
	gchar *tmp;
	HTMLURL *tmpurl;

	if(s == NULL || *s == 0)
		return NULL;

	if (s[0] == '#') {
		tmpurl = html_url_dup (baseURL, HTML_URL_DUP_NOREFERENCE);
		html_url_set_reference (tmpurl, s + 1);

		tmp = html_url_to_string (tmpurl);
		html_url_destroy (tmpurl);

		return tmp;
	}

	tmpurl = html_url_new (s);
	if (html_url_get_protocol (tmpurl) == NULL) {
		if (s[0] == '/') {
			if (s[1] == '/') {
				gchar *t;

				/* Double slash at the beginning.  */

				/* FIXME?  This is a bit sucky.  */
				t = g_strconcat (html_url_get_protocol (baseURL),
						 ":", s, NULL);
				html_url_destroy (tmpurl);
				tmpurl = html_url_new (t);
				retval = html_url_to_string (tmpurl);
				html_url_destroy (tmpurl);
				g_free (t);
			} else {
				/* Single slash at the beginning.  */

				html_url_destroy (tmpurl);
				tmpurl = html_url_dup (baseURL,
						       HTML_URL_DUP_NOPATH);
				html_url_set_path (tmpurl, s);
				retval = html_url_to_string (tmpurl);
				html_url_destroy (tmpurl);
			}
		} else {
			html_url_destroy (tmpurl);
			tmpurl = html_url_append_path (baseURL, s);
			retval = html_url_to_string (tmpurl);
			html_url_destroy (tmpurl);
		}
	} else {
		retval = html_url_to_string (tmpurl);
		html_url_destroy (tmpurl);
	}

	return retval;
}

struct _HTStream {
  BrowserInfo *bi;
  GtkHTMLStream *handle;
};

void title_changed_cb (GtkHTML *html, const gchar *title, BrowserInfo *bi)
{
	gchar *s;

	s = g_strconcat ("Encompass: ", title, NULL);
	gtk_window_set_title (GTK_WINDOW (window), s);
	g_free (s);
}

void entry_goto_selected_url(GtkWidget *widget, gpointer data) {
  gchar *url;
  go_item *item;
  
  item = g_list_nth_data(go_list, go_position);
  gtk_widget_hide (GTK_COMBO (entry)->popwin);
  gtk_grab_remove (GTK_COMBO (entry)->popwin);

  url = g_strdup(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(entry)->entry)));
  if(url && strlen(url)) {
    if(!strcmp(url, item->url)) {
    }
    else {
 	goto_url(url, 0);
     }
  }
  g_free(url);
}

void entry_goto_url(GtkWidget *widget, gpointer data)
{
	gchar *tmpurl, *url, *oldurl;
	int tmp;

	tmpurl = g_strdup (gtk_entry_get_text(GTK_ENTRY(widget)));
	
	if(tmpurl && strlen(tmpurl)) {

	if(strchr(tmpurl, ':')) {
	  url = g_strdup(tmpurl);
	  g_free(tmpurl);
	}
	else {
	  if(!strncmp(tmpurl, "ftp.", strlen("ftp."))) {
	    url = g_strdup_printf("ftp://%s", tmpurl);
	    on_set_base(NULL, url, bi);
	    g_free(tmpurl);
	  }
	  else if(!strncmp(tmpurl, "/", strlen("/"))) {
	    url = g_strdup_printf("file:%s", tmpurl);
	    on_set_base(NULL, url, bi);
	    g_free(tmpurl);
	  }
	  else {
	    if(!strchr(tmpurl, '/')) {
	      url = g_strdup_printf("http://%s/", tmpurl);
	    } else {
	      url = g_strdup_printf("http://%s", tmpurl);
	    }
	    on_set_base (NULL, url, bi);
	    g_free(tmpurl);
	  }
	}
	entry_list = g_list_prepend(entry_list, g_strdup(url));
	/* Removes old entries if the list is to big */
	tmp = g_list_length(entry_list);
	while(tmp > MAX_URL_ENTRIES) {
	  oldurl = g_list_nth_data(entry_list, MAX_GO_ENTRIES);

	  g_free(oldurl);

	  entry_list = g_list_remove(entry_list, oldurl);
	  tmp--;
	}
	gtk_combo_set_popdown_strings(GTK_COMBO(entry), entry_list);
	goto_url(url, 0);
	}
}

void home_cb (GtkWidget *widget, gpointer data)
{
  if(eprefs.home_url && strlen(eprefs.home_url)) {
    goto_url(eprefs.home_url, 0);
  }
  if(timer_id) {
    gtk_timeout_remove(timer_id);
  }
}

void back_cb (GtkWidget *widget, gpointer data)
{
	go_item *item;

	go_position++;

	if((item = g_list_nth_data(go_list, go_position))) {

		goto_url(item->url, 1);
		gtk_widget_set_sensitive(popup_menu_forward, TRUE);
		gtk_widget_set_sensitive(toolbar_forward, TRUE);
		set_fore_menu(TRUE);

		if(go_position == (g_list_length(go_list) - 1)) {

			gtk_widget_set_sensitive(popup_menu_back, FALSE);
			gtk_widget_set_sensitive(toolbar_back, FALSE);
			set_back_menu(FALSE);
		}

	} else
		go_position--;
}

void forward_cb (GtkWidget *widget, gpointer data)
{
	go_item *item;

	go_position--;

	if((go_position >= 0) && (item = g_list_nth_data(go_list, go_position))) {

		goto_url(item->url, 1);

		gtk_widget_set_sensitive(popup_menu_back, TRUE);
		gtk_widget_set_sensitive(toolbar_back, TRUE);
		set_back_menu(TRUE);

		if(go_position == 0) {
			gtk_widget_set_sensitive(popup_menu_forward, FALSE);
			gtk_widget_set_sensitive(toolbar_forward, FALSE);
			set_fore_menu(FALSE);
		}
	} else
		go_position++;
}

void reload_cb (GtkWidget *widget, gpointer data)
{
	go_item *item;

	if((item = g_list_nth_data(go_list, go_position))) {

		goto_url(item->url, 1);
	}
}

void stop_cb (GtkWidget *widget, gpointer data)
{
	/* Kill all requests */
	html_stream_handle = NULL;
}

void load_done (GtkHTML *html)
{
	gnome_animator_stop (GNOME_ANIMATOR (animator));
	gnome_animator_goto_frame (GNOME_ANIMATOR (animator), 1);
	gtk_widget_set_sensitive(reload_tb, TRUE);
	gtk_widget_set_sensitive(stop_tb, FALSE);
	gnome_snd_event("load_done");
}

static void
save_image_cb (GtkWidget *widget,
	       HTMLImage *image)
{
  char *filename;

  filename = strrchr (image->image_ptr->url, '/');
  if (filename == NULL)
    filename = image->image_ptr->url;
  else
    filename++;

  /* Keep the pixbuf around after the image has been destroyed */
  gdk_pixbuf_ref (image->image_ptr->pixbuf);
  save_pixbuf_as (image->image_ptr->pixbuf, filename);
}

static void set_background_cb(GtkWidget * widget, HTMLImage * image) {
  gchar * filename, * path, * fileset;

  /* Keep the pixbuf around after the image has been destroyed */
  gdk_pixbuf_ref (image->image_ptr->pixbuf);

  filename = g_strconcat(gnome_util_home_file("encompass.d"),
		     "/Encompass-Wallpaper.png", NULL);
  save_pixbuf (image->image_ptr->pixbuf, filename);

  fileset = g_strconcat (g_get_home_dir(), "/.gnome/Background", NULL);
  path = g_strconcat ("=", fileset, "=/Default/wallpaper", NULL);
  gnome_config_set_string (path,
			   fileset ? fileset : "none");
  g_free (path);

  gnome_config_sync();

  g_free (fileset);

  fileset = g_strdup_printf("background-properties-capplet -b \"%s\" --init-session-settings", filename);
  system (fileset);
  g_free (fileset);
  g_free (filename);
}

static void view_image_callback(GtkWidget *widget, gchar *url) {
  if(url && strlen(url)) {
    goto_url(url, 0); 
  }
}

static void copy_link_callback(GtkWidget *widget, gchar *url) {
  if(url && strlen(url)) {
    gint have_selection;

    /* FIXME: free previous data? */
    gtk_object_set_data (GTK_OBJECT (window), "selection", g_strdup (url));
    have_selection = gtk_selection_owner_set (window, GDK_SELECTION_PRIMARY,
	       GDK_CURRENT_TIME);
    if (!have_selection) g_warning("Selection not found");
  }
}

void dict_lookup_cb (GtkWidget *widget, gchar *data) {
  if(data && strlen(data)) {
    gchar * lookup;
    lookup = g_strconcat("http://work.ucsd.edu:5141/cgi-bin/http_webster?",
			 "method=exact&isindex=",
			 data, NULL);
    goto_url(lookup, 0);
  }
}

int on_button_press_event (GtkWidget *widget, GdkEventButton *event)
{
  GtkMenu *menu;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  /* The "widget" is the menu that was supplied when 
   * gtk_signal_connect_object was called.
   */
  menu = GTK_MENU (popup_menu);
  
  if (event->type == GDK_BUTTON_PRESS) {
    HTMLObject *object, *copy_obj;
    static GtkWidget *menu_item = NULL;
    static GtkWidget *copy_item = NULL;
    static GtkWidget *view_item = NULL;
    static GtkWidget *cp_item = NULL;
    static GtkWidget *dict_item = NULL;
    static GtkWidget *setbg_item = NULL;
    guint offset;

    if (event->button == 3) {
      object = html_engine_get_object_at (GTK_HTML (bi->htmlw)->engine,
					  event->x, event->y, &offset, FALSE);
      if (menu_item != NULL) {
	gtk_widget_destroy (menu_item);
	menu_item = NULL;
      }
      if (view_item != NULL) {
	gtk_widget_destroy (view_item);
	view_item = NULL;
      }
      if (setbg_item != NULL) {
	gtk_widget_destroy (setbg_item);
	setbg_item = NULL;
      }

      if (object != NULL && HTML_OBJECT_TYPE (object) == HTML_TYPE_IMAGE) {
        gchar *tmp_url;
        HTMLImage *image = HTML_IMAGE (object);
        
 	menu_item = gtk_menu_item_new_with_label (_("Save image..."));
	gtk_menu_append (GTK_MENU (popup_menu), menu_item);
	gtk_widget_show (menu_item);
	gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
			    GTK_SIGNAL_FUNC (save_image_cb), image);

        tmp_url = g_strdup(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(entry)->entry)));
        if(!strcmp(parse_href(image->image_ptr->url), tmp_url)) {
        } else {
	  view_item = gtk_menu_item_new_with_label (_("View image..."));
	  gtk_menu_append (GTK_MENU (popup_menu), view_item);
	  gtk_widget_show (view_item);
	  gtk_signal_connect (GTK_OBJECT (view_item), "activate",
			      view_image_callback, image->image_ptr->url);
	}
	g_free(tmp_url);

	setbg_item = gtk_menu_item_new_with_label (_("Set as background"));
	gtk_menu_append (GTK_MENU(popup_menu), setbg_item);
	gtk_widget_show (setbg_item);
	gtk_signal_connect (GTK_OBJECT (setbg_item), "activate",
			    GTK_SIGNAL_FUNC(set_background_cb), image);
      }
      copy_obj = html_engine_get_object_at (GTK_HTML (bi->htmlw)->engine,
					    event->x, event->y, &offset,
					    FALSE);
      if (copy_item != NULL) {
	gtk_widget_destroy (copy_item);
	copy_item = NULL;
      }
      if (copy_obj != NULL && HTML_OBJECT_TYPE (copy_obj) == HTML_TYPE_LINKTEXT) {
        gchar *tmp_url;
        HTMLLinkText *textmstr = HTML_LINK_TEXT(copy_obj);
        
        tmp_url = g_strdup(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(entry)->entry)));
        if(!strcmp(parse_href(textmstr->url), tmp_url)) {
        } else {
	  copy_item = gtk_menu_item_new_with_label (_("Copy Link Location"));
	  gtk_menu_append (GTK_MENU (popup_menu), copy_item);
	  gtk_widget_show (copy_item);
	  gtk_signal_connect (GTK_OBJECT (copy_item), "activate",
			      copy_link_callback, parse_href(textmstr->url));
	}
	g_free(tmp_url);
      }
      if (cp_item != NULL) {
	gtk_widget_destroy(cp_item);
	cp_item = NULL;
      }
      if (dict_item != NULL) {
	gtk_widget_destroy(dict_item);
	dict_item = NULL;
      }
      if (html_engine_is_selection_active(GTK_HTML(bi->htmlw)->engine)) {
	  cp_item = gtk_menu_item_new_with_label (_("Copy to Clipboard"));
	  gtk_menu_append (GTK_MENU (popup_menu), cp_item);
	  gtk_widget_show (cp_item);
	  gtk_signal_connect (GTK_OBJECT (cp_item), "activate",
			      copy_link_callback,
			      html_engine_get_selection_string(GTK_HTML(bi->htmlw)->engine));

	  if(!strchr(html_engine_get_selection_string(GTK_HTML(bi->htmlw)->engine), ' ')) {
	    gchar * blah;
	    dict_item = gtk_menu_item_new_with_label(_("Look up word..."));
	    blah = html_engine_get_selection_string(GTK_HTML(bi->htmlw)->engine);
	    gtk_menu_append (GTK_MENU(popup_menu), dict_item);
	    gtk_widget_show (dict_item);
	    gtk_signal_connect (GTK_OBJECT (dict_item), "activate",
				dict_lookup_cb, g_strdup(blah));
	    g_free(blah);
	  }
      }
      gtk_menu_popup (menu, NULL, NULL, NULL, NULL,
		      event->button, event->time);
      return TRUE;
    }
  }
  
  return FALSE;
}

void on_set_base (GtkHTML *html, const gchar *base_url, BrowserInfo *bi)
{
  gchar * helpindex, * lang;
  gtk_entry_set_text (GTK_ENTRY(GTK_COMBO(entry)->entry), base_url);

  if(!g_strncasecmp(base_url, "ghelp:", strlen("ghelp:"))) {
    HTMLURL * real_url;

    real_url = html_url_new(base_url);
    if(real_url->path[0] != '/') {
      lang = getenv("LANGUAGE");
      if(!lang)
	lang = getenv("LANG");
      if(!lang)
	lang = g_strdup("C");

      helpindex = g_strconcat(GNOME_HELP_DIR, "/", real_url->path,
			      "/", lang, "/index.html", NULL);
      base_url = g_strdup(g_strconcat("ghelp:", helpindex, NULL));
      g_free(helpindex);
    }
    g_free(real_url);
  }
  if (baseURL)
    html_url_destroy (baseURL);

  baseURL = html_url_new (base_url);

}

void browser_set_base_target(GtkWidget *htmlw, const char *base_target_url,
			     BrowserInfo *bi)
{
  if(bi->base_target_url)
    g_free(bi->base_target_url);
  bi->base_target_url = g_strdup(base_target_url);
}

gboolean redirect_timer_event (gpointer data) {
	goto_url(redirect_url, 0);

	/*	OBS: redirect_url is freed in goto_url */

	return FALSE;
}

void on_redirect (GtkHTML *html, const gchar *url, int delay, gpointer data) {

	if(redirect_timerId == 0) {

		redirect_url = g_strdup(url);
		
	redirect_timerId = gtk_timeout_add (delay * 1000,(GtkFunction) redirect_timer_event, NULL);
	}
}

static void read_block(GtkHTMLStream * handle,
				    const char * buf,
				    size_t len) {
//  fprintf(stderr,"* readblock: %i (%i)\n",len,handle);
  lock_gtk;
  if (len) {
    gtk_html_write(html, handle, buf, len);
  }
  unlock_gtk;
//  fprintf(stderr,"* readblock done: %i\n",len);
}

void on_submit (GtkHTML *htmlw, const gchar *method, const gchar *action,
	const gchar *encoding, BrowserInfo *bi) {

  GString *tmpstr = g_string_new (action);

  if(strcasecmp(method, "GET") == 0) {
    gchar *blah;
    blah = g_strconcat(action, "?", encoding, NULL);
    goto_url(blah, 0);
    g_free(blah);
  }
  /*
  else if(strcasecmp(method, "POST") == 0) {
    HTMLURL *tmpurl;
    gchar *realurl;
    ne_session * sess;
    ne_request * request;
    gchar *post_body;
    gchar **pieces = g_strsplit(encoding, "&", -1);

    if(pieces) {
      char *ctmp;
      ctmp = g_strjoinv("\r\n", pieces);
      printf("%s\r\n", ctmp);
      post_body = g_strdup(g_strconcat(ctmp, "\r\n", NULL));
      g_free(ctmp);
      realurl = parse_href(action);
      tmpurl = html_url_new(realurl);
      if(tmpurl->port == 0) tmpurl->port = 80;
      if(!tmpurl->hostname) {
	return;
      }
      sess = ne_session_create();

      ne_session_server(sess, tmpurl->hostname, 80);
      ne_set_useragent(sess, g_strconcat("Encompass ", VERSION,
					 " (compatible; Mozilla/4.7)", NULL));
      request = ne_request_create(sess, "POST", tmpurl->path);
      html_stream_handle = gtk_html_begin(html);
      ne_add_response_body_reader(request, ne_accept_always,
				  (void *)read_block, NULL);
      ne_set_request_body_buffer(request, post_body, sizeof(post_body));
      ne_request_dispatch(request);
      gtk_html_end(html, html_stream_handle, GTK_HTML_STREAM_OK);
      ne_request_destroy(request);
      ne_session_destroy(sess);
    }
  }
  */
  else {
    g_warning ("Unsupported submit method '%s'\n", method);
  }
  g_string_free (tmpstr, TRUE);
}

void on_url (GtkHTML *html, const gchar *url, BrowserInfo *bi)
{
  if (url)
    gnome_appbar_set_status (GNOME_APPBAR(bar), url);
  else
    gnome_appbar_set_status (GNOME_APPBAR(bar), "");
}

void on_link_clicked (GtkHTML *html, const gchar *url, gpointer data)
{
  if(strchr(url, '#')) {
  }
  else {
    goto_url (url, 0);
  }
}

static void list_file_data(GList * dlist, gchar * path,
			   GtkHTMLStream * handle) {
  gchar * html_data;
  GnomeVFSFileInfo * item;
  gint i;

  html_data = g_strconcat("<html>\n<head>\n<title>\n",
			  path, "\n</title>\n</head>\n<body>\n",
			  NULL);
  gtk_html_write(GTK_HTML(html), handle, html_data, strlen(html_data));
  g_free(html_data);
  for (i = 0; i < g_list_length(dlist); i++) {
    item = g_list_nth_data(dlist, i);
    html_data = g_strconcat("<a href=\"", path, "/", item->name,
			    "\">", item->name, "</a><br>\n", NULL);
    gtk_html_write(GTK_HTML(html), handle, html_data, strlen(html_data));
    g_free(item);
    g_free(html_data);
  }
  html_data = g_strdup("\n</body>\n</html>\n");
  gtk_html_write(GTK_HTML(html), handle, html_data, strlen(html_data));
  g_free(html_data);
  gtk_html_end(GTK_HTML(html), handle, GTK_HTML_STREAM_OK);
}

typedef struct {
  HTMLURL * htmlurl;
  GtkHTMLStream * handle;
} ThreadData;

static void load_file_cb(HTMLURL * url, GtkHTMLStream * handle)
{
  int fp;
  gchar *buffer;
  struct stat stbuf;

  if(!lstat(url->path, &stbuf)) {
    if(opendir(url->path)) {
      GList * dlist;
      GnomeVFSDirectoryFilter * filter = NULL;
      gchar * path;

      path = g_strconcat("file:", url->path, NULL);
      gnome_vfs_directory_list_load(&dlist, path,
				    GNOME_VFS_FILE_INFO_DEFAULT,
				    filter);
      list_file_data(dlist, path, handle);
      g_free(path);
    } else if(opendir(url->path) == NULL && 
       (fp = open(url->path, O_RDONLY | O_NONBLOCK))) {
      buffer = (char *)mmap(NULL, stbuf.st_size, PROT_READ,
			    MAP_SHARED, fp, 0);

      gtk_html_write(GTK_HTML(html), handle, buffer, stbuf.st_size);
      close(fp);
      munmap(buffer, stbuf.st_size);

      gtk_html_end(GTK_HTML(html), handle, GTK_HTML_STREAM_OK);
    }
  }
}

static void load_ghelp(HTMLURL * real_url, GtkHTMLStream * handle)
{
  gchar * helpindex, * lang;

  if (real_url->path[0] != '/') {
    lang = getenv("LANGUAGE");
    if(!lang)
      lang = getenv("LANG");
    if(!lang)
      lang = g_strdup("C");

    helpindex = g_strconcat(GNOME_HELP_DIR, "/", real_url->path, 
			    "/", lang, "/index.html", NULL);
    if(!g_file_exists(helpindex)) {
      lang = g_strdup("C");
      g_free(helpindex);
      helpindex = g_strconcat(GNOME_HELP_DIR, "/", real_url->path, 
			      "/", lang, "/index.html", NULL);
      if(!g_file_exists(helpindex)) {
	g_warning("Could not load: %s:%s\n", real_url->protocol,
		  helpindex);
	return;
      }
    }
    real_url->path = g_strdup(helpindex);
    load_file_cb(real_url, handle);
    g_free(helpindex);
  } else {
    load_file_cb(real_url, handle);
  }
}

gboolean event_loop = FALSE;
gint counter = 0;

void* ne_load_url(void * data)
{
  HTMLURL * htmlurl = html_url_dup(((ThreadData *) data)->htmlurl,HTML_URL_DUP_ALL);
  GtkHTMLStream * handle = ((ThreadData *) data)->handle;

  ne_session * sess;
  ne_request * request;

//  fprintf(stderr,"* Loading: %s://%s%s (%li %i)\n",htmlurl->protocol,htmlurl->hostname,htmlurl->path,handle,counter);
  g_free (data);
  g_mutex_unlock(data_mtx);

  sess = ne_session_create();
  ne_session_server(sess, htmlurl->hostname, htmlurl->port);
  ne_set_useragent(sess, "Mozilla/5 compatible; Encompass 1.0");
  if (htmlurl->port == 443) {
    ne_set_secure(sess, TRUE);
  }
  request = ne_request_create(sess, "GET", htmlurl->path);

  ne_add_response_body_reader(request, ne_accept_always,
		  (void *)read_block, handle);

  ne_request_dispatch(request);
  ne_request_destroy(request);
  ne_session_destroy(sess);

  lock_gtk;
  gtk_html_end(GTK_HTML(html), handle, GTK_HTML_STREAM_OK);
  unlock_gtk;
  g_mutex_lock(counter_mtx);
  counter--;
  g_mutex_unlock(counter_mtx);
//  fprintf(stderr,"* Loading done: %s://%s%s (%li)\n",htmlurl->protocol,htmlurl->hostname,htmlurl->path,handle);
  return (void *)0;
}

static void load_url(HTMLURL * htmlurl, GtkHTMLStream * handle)
{
  ThreadData * data = g_new(ThreadData, 1);
  pthread_t load_thread;
  gboolean start_loop;

//  fprintf(stderr,"* URL Requested: %s://%s%s\n",htmlurl->protocol,htmlurl->hostname,htmlurl->path);
  if(htmlurl->port == 0) {
    if (!strcasecmp(htmlurl->protocol, "http"))
      htmlurl->port = 80;
    if (!strcasecmp(htmlurl->protocol, "https"))
      htmlurl->port = 443;
  }

  data->htmlurl = htmlurl;
  data->handle = handle;
  
  g_mutex_lock(counter_mtx);
  counter++;
  g_mutex_unlock(counter_mtx);
  g_mutex_lock(event_loop_mtx);
  start_loop = !event_loop;
  if (start_loop) {
    event_loop = TRUE;
  }
  g_mutex_unlock(event_loop_mtx);
  g_mutex_lock(data_mtx);
  pthread_create(&load_thread, NULL, ne_load_url, data);
  g_mutex_lock(data_mtx);
  g_mutex_unlock(data_mtx);
  if (start_loop) {
    g_mutex_lock(counter_mtx);
    while (counter > 0) {
//      fprintf(stderr,"* counter: %i\n",counter);
      g_mutex_unlock(counter_mtx);
      lock_gtk;
      while(gtk_events_pending()) {
        gtk_main_iteration();
      }
      unlock_gtk;
      sleep(0);
      g_mutex_lock(counter_mtx);
    }
    g_mutex_unlock(counter_mtx);
    g_mutex_lock(event_loop_mtx);
    event_loop = FALSE;
    g_mutex_unlock(event_loop_mtx);
    sleep(0);
  }
//  ne_load_url(data);
//  fprintf(stderr,"* URL Requested done: %s://%s%s\n",htmlurl->protocol,htmlurl->hostname,htmlurl->path);
}

void url_requested (GtkHTML *html, const char *url,
		    GtkHTMLStream *handle, BrowserInfo *bi)
{
  HTMLURL * htmlurl = html_url_new(parse_href(url));

  if(!strcasecmp(htmlurl->protocol, "file")) {
    load_file_cb(htmlurl, handle);
  } else if(!strcasecmp(htmlurl->protocol, "ghelp")) {
    load_ghelp(htmlurl, handle);
  } else {
    load_url(htmlurl, handle);
  }
  html_url_destroy(htmlurl);
}

void go_list_cb (GtkWidget *widget, gpointer data)
{
	go_item *item;
	int num;
	/* Only if the item was selected, not deselected */
	if(GTK_CHECK_MENU_ITEM(widget)->active) {
		
		go_position = GPOINTER_TO_INT(data);
		
		if((item = g_list_nth_data(go_list, go_position))) {
			
			goto_url(item->url, 1);
			num = g_list_length(go_list);

			if(go_position == 0 || num < 2) {
				gtk_widget_set_sensitive(popup_menu_forward, FALSE);
				gtk_widget_set_sensitive(toolbar_forward, FALSE);
				set_fore_menu(FALSE);
			} else {
				gtk_widget_set_sensitive(popup_menu_forward, TRUE);
				gtk_widget_set_sensitive(toolbar_forward, TRUE);
				set_fore_menu(TRUE);
			}
			if(go_position == (num - 1) || num < 2) {
				gtk_widget_set_sensitive(popup_menu_back, FALSE);
				gtk_widget_set_sensitive(toolbar_back, FALSE);
				set_back_menu(FALSE);
			} else {
				gtk_widget_set_sensitive(popup_menu_back, TRUE);
				gtk_widget_set_sensitive(toolbar_back, TRUE);
				set_back_menu(TRUE);
			}
		}
  }
}

void remove_go_list(gpointer data, gpointer user_data) {
	go_item *item = (go_item *)data;
	
	if(item->widget)
		gtk_widget_destroy(item->widget);
	
	item->widget = NULL;
}

void goto_url(const char *url, int back_or_forward)
{
	int tmp, i;
	go_item *item;
	GSList *group = NULL;
	gchar *full_url;
	gchar *img_page;

  full_url = parse_href(g_strdup(url));

  if(!strncmp(url, "mailto:", strlen("mailto:"))) {
    gnome_url_show(url);
  }
  else if (!strncmp(url, "ftp://", strlen("ftp://")) ||
    !strcmp("x-url/ftp", gnome_mime_type(url))) {
    gnome_url_show(url);
  }
  else if (!strncmp(url, "man:", strlen("man:")) ||
    !strncmp(url, "info:", strlen("info:"))) {
      gnome_url_show(url);
  }
  else {
	/* Kill all requests */

	/* Remove any pending redirection */
	if(redirect_timerId) {
		gtk_timeout_remove(redirect_timerId);

		redirect_timerId = 0;
	}

	use_redirect_filter = TRUE;


  if(!strchr(url, '?') && !strncmp(gnome_mime_type(url), "image/", strlen("image/"))) {
    html_stream_handle = gtk_html_begin(GTK_HTML(html));
    img_page = g_strconcat("<title>Image: ", parse_href(url), "</title>\n<img src=\"",
    	url, "\">\n", NULL);
    gtk_html_write(GTK_HTML(html), html_stream_handle, img_page, strlen(img_page));
    gtk_html_end(GTK_HTML(html), html_stream_handle, GTK_HTML_STREAM_OK);
  }
  else {
    html_stream_handle = gtk_html_begin(GTK_HTML(html));
    /* Yuck yuck yuck.  Well this code is butt-ugly already anyway.  */
    on_set_base(html, full_url, bi);
    url_requested (html, full_url, html_stream_handle, bi);
  }

	  on_set_base (NULL, full_url, bi);

	if(!back_or_forward) {
		if(go_position) {
			/* Removes "Forward entries"*/
			tmp = go_position;
			while(tmp) {
				item = g_list_nth_data(go_list, --tmp);
				go_list = g_list_remove(go_list, item);
				if(item->url)
					g_free(item->url);
				if(item->title)
					g_free(item->title);
				if(item->url)
					gtk_widget_destroy(item->widget);
				g_free(item);
			}
			go_position = 0;
		}

		/* Removes old entries if the list is to big */
		tmp = g_list_length(go_list);
		while(tmp > MAX_GO_ENTRIES) {
			item = g_list_nth_data(go_list, MAX_GO_ENTRIES);

			if(item->url)
				g_free(item->url);
			if(item->title)
				g_free(item->title);
			if(item->url)
				gtk_widget_destroy(item->widget);
			g_free(item);

			go_list = g_list_remove(go_list, item);
			tmp--;
		}
		gtk_widget_set_sensitive(popup_menu_forward, FALSE);
		gtk_widget_set_sensitive(toolbar_forward, FALSE);
		set_fore_menu(FALSE);
		
		item = g_malloc0(sizeof(go_item));
		item->url = g_strdup(full_url);
		item->title = g_strdup(gtk_html_get_title(html));

		/* Remove old go list */
		g_list_foreach(go_list, remove_go_list, NULL);

		/* Add new url to go list */
		go_list = g_list_prepend(go_list, item);

		/* Create a new go list menu */
		tmp = g_list_length(go_list);
		group = NULL;

		for(i=0;i<tmp;i++) {
			item = g_list_nth_data(go_list, i);
			item->widget = gtk_radio_menu_item_new_with_label(group, item->url);

			gtk_signal_connect (GTK_OBJECT (item->widget), "activate",
					    GTK_SIGNAL_FUNC (go_list_cb), GINT_TO_POINTER (i));

			group = gtk_radio_menu_item_group(GTK_RADIO_MENU_ITEM(item->widget));

			if(i == 0)
				gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item->widget), TRUE);

			go_menu_append(item->widget);
			gtk_widget_show(item->widget);
			
		}
		/* Enable the "Back" button if there are more then one url in the list */
		if(g_list_length(go_list) > 1) {

			gtk_widget_set_sensitive(popup_menu_back, TRUE);
			gtk_widget_set_sensitive(toolbar_back, TRUE);
			set_back_menu(TRUE);
		}
	} else {
		/* Update current link in the go list */
		item = g_list_nth_data(go_list, go_position);
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item->widget), TRUE);
	}

	if(redirect_url) {

		g_free(redirect_url);
		redirect_url = NULL;
	}
	g_free (full_url);
  }
}
