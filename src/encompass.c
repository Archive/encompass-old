#include "config.h"
#include <gtkhtml/gtkhtml-embedded.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <bonobo.h>

static void prepare_app(void) {
  int i;
    
  gnome_config_push_prefix("Encompass/");

  for (i = 0; i < MAX_URL_ENTRIES; i++) {
    gchar *s = g_strdup_printf ("URLBarHistory/entry%d", i);
    gchar *entry_str;

    entry_str = gnome_config_get_string (s);
    if(entry_str && *entry_str != '\0') {
      /* Quicker to Prepend and then reverse :) */
      entry_list = g_list_prepend(entry_list, entry_str);
    }
    g_free (s);
  }

  entry_list = g_list_reverse (entry_list);
  if(entry_list != NULL) {
    gtk_combo_set_popdown_strings(GTK_COMBO(entry), entry_list);
    gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(entry)->entry), "");
  }

  eprefs.SoundEnabled = gnome_config_get_int("Options/SoundEnabled=1");
  eprefs.home_url = gnome_config_get_string("Options/HomeURL=http://www.ximian.com/");
  eprefs.search_url = gnome_config_get_string("Options/SrchURL=http://www.yahoo.com/");
  gnome_config_pop_prefix();
}

static struct poptOption options[] = {
  { "newwin", 'n', POPT_ARG_NONE, &newwin, 0, N_("open in new window"), NULL},
  {NULL}
};

static int delete_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
  if(windows > 1) {
    windows--;
  }
  else {
    file_quit_callback(NULL, NULL);
  }

  /* Return TRUE to destroy the window that got the event */
  return TRUE;
}

static gboolean object_requested_cmd (GtkHTML *html, GtkHTMLEmbedded *eb, 
				      void *data) {

  if(!eb->classid)
    return FALSE;

  if(!strncasecmp(eb->classid, "OAFIID:", strlen("OAFIID:"))) {
    GtkWidget * component = bonobo_widget_new_control(eb->classid,
	      CORBA_OBJECT_NIL);
    gtk_widget_show(component);
    gtk_container_add(GTK_CONTAINER(eb), component);
  }
  printf("CLASS ID: %s\n", eb->classid);
  printf("Object Size: %dx%d\n", eb->width, eb->height);
  if(!eb->type && gtk_html_embedded_get_parameter(eb, "src")) {
    gchar * path = gtk_html_embedded_get_parameter(eb, "src");
    eb->type = (gchar *)gnome_vfs_get_file_mime_type(path, NULL, TRUE);
    g_free(path);
  }
  printf("Object type: %s\n", eb->type);
  return TRUE;
}

static void window_selection_get_cb (GtkWidget *widget,
				     GtkSelectionData *selection_data,
				     guint info, guint time_stamp,
				     gpointer data ) {
	gchar *text;

	text = gtk_object_get_data (GTK_OBJECT(window), "selection");
	g_return_if_fail (text != NULL);

	gtk_selection_data_set (selection_data, GDK_SELECTION_TYPE_STRING,
				8, text, strlen (text));
}

static void create_window(EncompassWindow *ewin) {
	GtkWidget *app;
	GtkWidget *html_widget;
	GtkWidget *scrolled_window;
	GdkColor   bgColor = {0, 0xffff, 0xffff, 0xffff};
	gchar *e_icon;
	
	gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
	gtk_widget_set_default_visual (gdk_rgb_get_visual ());
	
	bi = g_new0(BrowserInfo, 1);

	window = app = ewin->window = gnome_app_new (_("Encompass"), _("Encompass: <Untitled>"));

	e_icon = gnome_pixmap_file("encompass.png");
	gnome_window_icon_set_from_file(GTK_WINDOW(window), e_icon);
	g_free(e_icon);
	gtk_signal_connect (GTK_OBJECT (app), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event), NULL);

	bar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
	gnome_app_set_statusbar (GNOME_APP (app), bar);
	create_toolbars (app);
	prepare_app();
	create_menu(app);

	/* Disable back and forward on the Go menu */
	set_back_menu(FALSE);
	set_fore_menu(FALSE);

	apptents = scrolled_window = gtk_scrolled_window_new (NULL, NULL);

	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	gnome_app_set_contents (GNOME_APP (app), scrolled_window);

	bi->htmlw = html_widget = gtk_html_new ();
	html = ewin->html = GTK_HTML (html_widget);
	gtk_html_load_empty (ewin->html);
	gtk_html_set_default_background_color (GTK_HTML (html_widget), &bgColor);
	
	gtk_container_add (GTK_CONTAINER (scrolled_window), html_widget);

	/* Create a popup menu with disabled back and forward items */
	popup_menu = gtk_menu_new();

	popup_menu_back = gtk_menu_item_new_with_label(_("Back"));
	gtk_widget_set_sensitive(popup_menu_back, FALSE);
	gtk_menu_append(GTK_MENU(popup_menu), popup_menu_back);
	gtk_widget_show(popup_menu_back);
	gtk_signal_connect (GTK_OBJECT (popup_menu_back), "activate",
			    GTK_SIGNAL_FUNC (back_cb), NULL);

	popup_menu_forward = gtk_menu_item_new_with_label(_("Forward"));
	gtk_widget_set_sensitive(popup_menu_forward, FALSE);
	gtk_menu_append(GTK_MENU(popup_menu), popup_menu_forward);
	gtk_widget_show(popup_menu_forward);
	gtk_signal_connect (GTK_OBJECT (popup_menu_forward), "activate",
			    GTK_SIGNAL_FUNC (forward_cb), NULL);

	popup_menu_home = gtk_menu_item_new_with_label(_("Home"));
	gtk_menu_append(GTK_MENU(popup_menu), popup_menu_home);
	gtk_widget_show(popup_menu_home);
	gtk_signal_connect (GTK_OBJECT (popup_menu_home), "activate",
			    GTK_SIGNAL_FUNC (home_cb), NULL);

	/* End of menu creation */

	gtk_widget_set_events (html_widget, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);

	gtk_signal_connect (GTK_OBJECT (ewin->html), "title_changed",
			    GTK_SIGNAL_FUNC (title_changed_cb), bi);

	gtk_signal_connect (GTK_OBJECT (ewin->html), "url_requested",
			    GTK_SIGNAL_FUNC (url_requested), bi);

	gtk_signal_connect (GTK_OBJECT (ewin->html), "load_done",
			    GTK_SIGNAL_FUNC (load_done), bi);
	gtk_signal_connect (GTK_OBJECT (ewin->html), "on_url",
			    GTK_SIGNAL_FUNC (on_url), bi);
	gtk_signal_connect (GTK_OBJECT (ewin->html), "set_base",
			    GTK_SIGNAL_FUNC (on_set_base), bi);
	gtk_signal_connect (GTK_OBJECT (ewin->html), "button_press_event",
			    GTK_SIGNAL_FUNC (on_button_press_event), bi);
	gtk_signal_connect_after (GTK_OBJECT (ewin->html), "link_clicked",
			    GTK_SIGNAL_FUNC (on_link_clicked), bi);
	gtk_signal_connect (GTK_OBJECT (ewin->html), "redirect",
			    GTK_SIGNAL_FUNC (on_redirect), bi);
	gtk_signal_connect (GTK_OBJECT (ewin->html), "submit",
			    GTK_SIGNAL_FUNC (on_submit), bi);
	gtk_signal_connect (GTK_OBJECT (html), "object_requested",
			    GTK_SIGNAL_FUNC (object_requested_cmd), bi);
	gtk_signal_connect(GTK_OBJECT(bi->htmlw), "set_base_target", browser_set_base_target, bi);

#if 0
	gtk_box_pack_start_defaults (GTK_BOX (hbox), GTK_WIDGET (ewin->html));
	vscrollbar = gtk_vscrollbar_new (GTK_LAYOUT (ewin->html)->vadjustment);
	gtk_box_pack_start (GTK_BOX (hbox), vscrollbar, FALSE, TRUE, 0);
	
#endif
	gtk_widget_realize (GTK_WIDGET (ewin->html));

	gtk_window_set_default_size (GTK_WINDOW (app), 500, 400);
	gtk_window_set_focus (GTK_WINDOW (app), GTK_WIDGET (ewin->html));

	windows++;
	gtk_widget_show_all (app);

	gtk_selection_add_target (window, GDK_SELECTION_PRIMARY, 
				  GDK_SELECTION_TYPE_STRING, 1);
	gtk_signal_connect (GTK_OBJECT(window), "selection_get",
		GTK_SIGNAL_FUNC (window_selection_get_cb), NULL);

}

gint main (gint argc, gchar *argv[])
{
  poptContext ctx;
  gchar *encompass_dir;
  CORBA_ORB orb;
#ifdef GTKHTML_HAVE_GCONF
  GError  *gconf_error  = NULL;
#endif
  const char **args;

  baseURL = NULL;

  encompass_dir = gnome_util_home_file("encompass.d");
  if(!g_file_exists(encompass_dir)) {
    mkdir(encompass_dir, 0755);
  }
  if(!g_file_test(encompass_dir, G_FILE_TEST_ISDIR)) {
    g_warning("~/.gnome/encompass.d is not a directory!\n");
    return 1;
  }
  g_free(encompass_dir);
  encompass_dir = gnome_util_home_file("encompass.d/cache");
  if(!g_file_exists(encompass_dir)) {
    mkdir(encompass_dir, 0755);
  }
  if(!g_file_test(encompass_dir, G_FILE_TEST_ISDIR)) {
    g_warning("~/.gnome/encompass.d/cache is not a directory!\n");
    return 1;
  }
  g_free(encompass_dir);

  bindtextdomain(PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain(PACKAGE);

  ewin = g_new0(EncompassWindow, 1);
  windows = 0;
  gnome_init_with_popt_table (PACKAGE, VERSION,	argc, argv, options, 0, &ctx);

  orb = oaf_init (argc, argv);

#ifdef GTKHTML_HAVE_GCONF
  if (!gconf_init (argc, argv, &gconf_error)) {
    g_assert (gconf_error != NULL);
    g_error ("GConf init failed:\n  %s", gconf_error->message);
    return FALSE;
  }
#endif

  g_thread_init(NULL);
  event_loop_mtx = g_mutex_new();
  counter_mtx = g_mutex_new();
  data_mtx = g_mutex_new();
  gtk_mtx = g_mutex_new();
  gdk_rgb_init ();
  sock_init();

  gnome_vfs_init();

  tooltips = gtk_tooltips_new();
  create_window(ewin);
  args = poptGetArgs(ctx);

  if (args && args[0]) {
    if(!strchr(args[0], ':')) {
      args[0] = g_strdup_printf("http://%s", args[0]);
    }
    goto_url(args[0], 0);
  }
  else if(eprefs.home_url && strlen(eprefs.home_url)) {
    timer_id = gtk_timeout_add(2000, (GtkFunction) home_cb, NULL);
  }

  if (bonobo_init (orb, NULL, NULL) == FALSE) {
    g_error ("Couldn't initialize Bonobo");
  }

  gdk_threads_enter();
  bonobo_main ();
  gdk_threads_leave();

  return 0;
}
