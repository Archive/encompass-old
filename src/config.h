#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <ne_request.h>
#include <ne_session.h>
#include <ne_basic.h>
#include <ne_redirect.h>
#include <ne_utils.h>
#include <ne_socket.h>
#include <ne_uri.h>

#undef PACKAGE
#undef VERSION

#include "../config.h"

#include <gnome.h>
#include <gtk/gtk.h>
#include <libgnomeui/gnome-window-icon.h>

#ifdef GTKHTML_HAVE_GCONF
  #include <gconf/gconf.h>
#endif

#include <sys/stat.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-print-master.h>
#include <libgnomeprint/gnome-print-master-preview.h>

#include <gtkhtml/gtkhtml.h>
#include <gtkhtml/htmlsettings.h>

#include <libgnomevfs/gnome-vfs.h>

#include "htmlurl.h"
#include "html.h"
#include "menu.h"
#include "toolbar.h"
#include "print.h"
#include "prefs.h"
#include "history.h"
#include "bookmarks.h"
#include "search.h"
#include "full-screen.h"

#include <pthread.h>

typedef struct {
  FILE *fil;
  GtkHTMLStream *handle;
} FileInProgress;

typedef struct {
	gchar *url;
	gchar *title;
	GtkWidget *widget;
} go_item; 

typedef struct {
  GtkWidget *window;
  GtkHTML *html;
  GtkHTMLStream *html_stream_handle;
} EncompassWindow;

#define MAX_GO_ENTRIES 20
#define MAX_URL_ENTRIES 20

GtkWidget *animator, *entry;
GtkWidget *popup_menu, *popup_menu_back, *popup_menu_forward, *popup_menu_home;
GtkWidget *toolbar_back, *toolbar_forward, *stop_tb, *reload_tb;

GList *go_list;
GList *entry_list;
int go_position;
guint timer_id;
int windows;
gchar *newwin;

EncompassWindow *ewin;
encomprefs eprefs;
ewidgets prefwidgets;

GtkHTML *html;
GtkHTMLStream *html_stream_handle;
GtkWidget *window, *bar;
BrowserInfo *bi;
HTMLURL *baseURL;
GtkWidget *apptents;

gboolean use_redirect_filter;

gint redirect_timerId;
gchar *redirect_url;
GtkTooltips *tooltips;

GMutex * event_loop_mtx;
GMutex * counter_mtx;
GMutex * data_mtx;
GMutex * gtk_mtx;
#endif
