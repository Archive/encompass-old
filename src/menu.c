#include "config.h"

void cancelbt_cb(GtkWidget *widget, gpointer data) {
	gnome_dialog_close(GNOME_DIALOG(widget->parent->parent->parent->parent->parent));
}

void file_quit_callback (GtkWidget *widget, gpointer data) {
  int i = 0 ;
  GList *list;
  
  gnome_config_push_prefix("Encompass/");
  
  list = entry_list;
  while (list && i < MAX_URL_ENTRIES) {
   gchar *s = g_strdup_printf("URLBarHistory/entry%d", i);

    if(list->data) {
      gnome_config_set_string(s, list->data);
    }

    g_free (s);
    list = list->next;
    i++;
  }  
  gnome_config_sync();
  gnome_config_pop_prefix();
  
  gnome_vfs_shutdown();
  sock_exit();
  gtk_main_quit();
}

static void close_about(GtkWidget *widget, gpointer data) {
  gnome_dialog_close(GNOME_DIALOG(widget));
}

static void help_about_callback (GtkWidget *widget, void *data) {
  static GtkWidget *about = NULL;
  const gchar *authors[] = {
    "Rodney Dawes <dobey@free.fr>",
    "Kevin Breit <battery841@mypad.com>",
    "Jan Van Buggenhout <chipzz@ulyssis.org>",
    "Francis Tyers <fran@thinkgeek.co.uk>",
    NULL
  };
  
  about = gnome_about_new (_("Encompass"),
			   VERSION,
			   /* copyright notice */
			   "Copyright 2000 (C) The Free Software Foundation",
			   authors,
			   /* other comments */
			   _("A Web browser for the Gnome desktop."),
			   "encompass/encompass-logo.jpg");
  gtk_signal_connect(GTK_OBJECT(about), "destroy",
		     GTK_SIGNAL_FUNC(close_about), NULL);
  gtk_widget_show (about);
  
  return;
}

static GnomeUIInfo file_menu [] = {
  {
    GNOME_APP_UI_ITEM, N_("_Print"),
    N_("Print the current page"),
    print_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PRINT,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("Print P_review..."),
    N_("See what the printed page would look like"),
    print_preview_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PRINT,
    0, 0, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_EXIT_ITEM (file_quit_callback, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[]=
{
  {
    GNOME_APP_UI_ITEM, N_("_Find in page"),
    N_("Find text in the current page"),
    find_in_page_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SEARCH,
    'F', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("Find a_gain"),
    N_("Find the next occurance of searched text"),
    find_again_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SEARCH,
    'G', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_PREFERENCES_ITEM(pref_dlg_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo view_menu[]= {
 {
   GNOME_APP_UI_ITEM,
   N_("Reload"), N_("Reload the current page"),
   reload_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_REFRESH,
   'R', GDK_CONTROL_MASK, NULL
 },
 {
   GNOME_APP_UI_ITEM,
   N_("Stop Loading"), N_("Stop loading the current page"),
   stop_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_STOP,
   GDK_Escape, 0, NULL
 },
 GNOMEUIINFO_END
};

static GnomeUIInfo go_menu[] = {
 {
   GNOME_APP_UI_ITEM,
   N_("Back"), N_("Return to the previous page in history list"),
   back_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BACK,
   GDK_Left, GDK_MOD1_MASK, NULL
 },
 {
   GNOME_APP_UI_ITEM,
   N_("Forward"), N_("Go to the next page in history list"),
   back_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_FORWARD,
   GDK_Right, GDK_MOD1_MASK, NULL
 },
 {
   GNOME_APP_UI_ITEM,
   N_("Home"), N_("Go to your home page"),
   back_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_HOME,
   GDK_Home, GDK_MOD1_MASK, NULL
 },
 GNOMEUIINFO_SEPARATOR,
 GNOMEUIINFO_END
};

static GnomeUIInfo bookmarks_menu[]= {
  {
    GNOME_APP_UI_ITEM, N_("Add Bookmark"),
    N_("Add current page to your bookmarks"),
    edit_bookmark_entry, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK,
    0, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM, N_("Edit Bookmarks"),
    N_("Edit your bookmarks"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK,
    0, 0, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_END
};
    
static GnomeUIInfo tool_menu[]=
  {
    GNOMEUIINFO_END
  };

static GnomeUIInfo help_menu[]=
{
  GNOMEUIINFO_HELP("encompass"),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_ABOUT_ITEM(help_about_callback, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE (edit_menu),
	GNOMEUIINFO_MENU_VIEW_TREE (view_menu),
	GNOMEUIINFO_SUBTREE (N_("_Go"), go_menu),
	GNOMEUIINFO_SUBTREE (N_("_Bookmarks"), bookmarks_menu),
	GNOMEUIINFO_SUBTREE (N_("_Tools"), tool_menu),
	GNOMEUIINFO_MENU_HELP_TREE (help_menu),
	GNOMEUIINFO_END
};

void create_menu(GtkWidget *app) {
  gnome_app_create_menus (GNOME_APP (app), main_menu);
  gnome_app_install_menu_hints (GNOME_APP (app), main_menu);
  gtk_widget_set_sensitive(bookmarks_menu[1].widget, FALSE);
  gtk_widget_set_sensitive(tool_menu[0].widget, FALSE);
}

void set_back_menu(gboolean sense) {
  gtk_widget_set_sensitive(go_menu[0].widget, sense);
}

void set_fore_menu(gboolean sense) {
  gtk_widget_set_sensitive(go_menu[1].widget, sense);
}

void go_menu_append(GtkWidget *widget) {
  gtk_menu_append(GTK_MENU(GTK_MENU_ITEM(main_menu[3].widget)->submenu), widget);
}

void parse_books(void) {
  parse_bookmarks_dir(gnome_util_home_file("apps"),
  	GTK_MENU(GTK_MENU_ITEM(main_menu[4].widget)->submenu));
}

GtkWidget *get_bm_menu(void) {
  return main_menu[4].widget;
}
