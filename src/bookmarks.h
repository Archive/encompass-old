#ifndef _BOOKMARKS_H_
#define _BOOKMARKS_H_

void edit_bookmark_entry(GtkWidget *widget, gchar *file);
void parse_bookmarks_dir(gchar *path, GtkMenu *menu);

#endif
