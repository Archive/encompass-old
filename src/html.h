#ifndef _HTML_H_
#define _HTML_H_

typedef struct {
  GtkWidget *htmlw;
  char *base_url, *base_target_url;

  int prevsel;

  char *post_data;
} BrowserInfo;

/* Lame hack */

void title_changed_cb (GtkHTML *html, const gchar *title, BrowserInfo *bi);
void entry_goto_selected_url(GtkWidget *widget, gpointer data);
void entry_goto_url(GtkWidget *widget, gpointer data);
void home_cb (GtkWidget *widget, gpointer data);
void back_cb (GtkWidget *widget, gpointer data);
void forward_cb (GtkWidget *widget, gpointer data);
void reload_cb (GtkWidget *widget, gpointer data);
void stop_cb (GtkWidget *widget, gpointer data);
void load_done (GtkHTML *html);
void dict_lookup_cb(GtkWidget * widget, gchar * data);
int on_button_press_event (GtkWidget *widget, GdkEventButton *event);
void on_set_base (GtkHTML *html, const gchar *url, BrowserInfo *bi);
void browser_set_base_target(GtkWidget *htmlw, const char *base_target_url, BrowserInfo *bi);
gboolean redirect_timer_event (gpointer data);
void on_redirect (GtkHTML *html, const gchar *url, int delay, gpointer data);
void on_submit (GtkHTML *html, const gchar *method, const gchar *action,
	const gchar *encoding, BrowserInfo *bi);
void on_url (GtkHTML *html, const gchar *url, BrowserInfo *bi);
void on_link_clicked (GtkHTML *html, const gchar *url, gpointer data);
void browser_goto_url(GtkWidget *htmlw, const char *url, BrowserInfo *bi);
void url_requested (GtkHTML *html, const char *url, GtkHTMLStream *handle, BrowserInfo *bi);
void go_list_cb (GtkWidget *widget, gpointer data);
void remove_go_list(gpointer data, gpointer user_data);
void goto_url(const char *url, int back_or_forward);

#endif
