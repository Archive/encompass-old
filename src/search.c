#include "config.h"
#include <gtkhtml/htmlengine-search.h>

struct _GtkHTMLSearchDialog {
	GnomeDialog *dialog;
	GtkHTML     *html;
	GtkWidget   *entry;
	GtkWidget   *backward;
	GtkWidget   *case_sensitive;

	gboolean     regular;
};

static void
search_cb (GtkWidget *but, GtkHTMLSearchDialog *d) {
  GtkWidget *mbox;
  
  if(!html_engine_search (d->html->engine, gtk_entry_get_text (GTK_ENTRY (d->entry)),
			    GTK_TOGGLE_BUTTON (d->case_sensitive)->active,
			    GTK_TOGGLE_BUTTON (d->backward)->active == 0, d->regular)) {
    mbox = gnome_message_box_new(_("Text not found."),
      GNOME_MESSAGE_BOX_INFO, GNOME_STOCK_BUTTON_OK, NULL);
    gnome_dialog_button_connect(GNOME_DIALOG(mbox), 0, cancelbt_cb, NULL);
    gtk_widget_show(mbox);
  }
}

static void
entry_changed (GtkWidget *entry, GtkHTMLSearchDialog *d)
{
}

static void
entry_activate (GtkWidget *entry, GtkHTMLSearchDialog *d)
{
	gnome_dialog_close (d->dialog);
	search_cb (NULL, d);
}

GtkHTMLSearchDialog *
gtk_html_search_dialog_new (GtkHTML *html, gboolean regular)
{
	GtkHTMLSearchDialog *dialog = g_new (GtkHTMLSearchDialog, 1);
	GtkWidget *hbox;

	dialog->dialog         = GNOME_DIALOG (gnome_dialog_new ((regular) ? _("Regex search") :  _("Search"), _("Search"),
								 GNOME_STOCK_BUTTON_CANCEL, NULL));
	dialog->entry          = gtk_entry_new_with_max_length (20);
	dialog->backward       = gtk_check_button_new_with_label (_("search backward"));
	dialog->case_sensitive = gtk_check_button_new_with_label (_("case sensitive"));
	dialog->html           = html;
	dialog->regular        = regular;

	hbox = gtk_hbox_new (FALSE, 0);

	gtk_box_pack_start_defaults (GTK_BOX (hbox), dialog->backward);
	gtk_box_pack_start_defaults (GTK_BOX (hbox), dialog->case_sensitive);

	gtk_box_pack_start_defaults (GTK_BOX (dialog->dialog->vbox), dialog->entry);
	gtk_box_pack_start_defaults (GTK_BOX (dialog->dialog->vbox), hbox);
	gtk_widget_show (dialog->entry);
	gtk_widget_show_all (hbox);

	gnome_dialog_button_connect (dialog->dialog, 0, search_cb, dialog);
	gnome_dialog_close_hides (dialog->dialog, TRUE);
	gnome_dialog_set_close (dialog->dialog, TRUE);

	gnome_dialog_set_default (dialog->dialog, 0);
	gtk_widget_grab_focus (dialog->entry);

	gtk_signal_connect (GTK_OBJECT (dialog->entry), "changed",
			    entry_changed, dialog);
	gtk_signal_connect (GTK_OBJECT (dialog->entry), "activate",
			    entry_activate, dialog);

	return dialog;
}

void
gtk_html_search_dialog_destroy (GtkHTMLSearchDialog *d)
{
	g_free (d);
}

void
search (GtkHTML *html, gboolean regular)
{
  GtkHTMLSearchDialog *dialog;
  
  dialog = gtk_html_search_dialog_new(html, regular);
  gtk_widget_show_all(GTK_WIDGET(dialog->dialog));
}

void
search_next (GtkHTML *html) {
  GtkWidget *mbox;
  
  if (html->engine->search_info) {
    if(!html_engine_search_next (html->engine)) {
      mbox = gnome_message_box_new(_("End of page reached. Continue from beginning?"),
	GNOME_MESSAGE_BOX_QUESTION, GNOME_STOCK_BUTTON_YES, GNOME_STOCK_BUTTON_NO, NULL);
      gnome_dialog_button_connect(GNOME_DIALOG(mbox), 0, find_again_cb, NULL);
      gnome_dialog_button_connect(GNOME_DIALOG(mbox), 1, cancelbt_cb, NULL);
      gtk_widget_show(mbox);
    }
  } else {
    search (html, TRUE);
  }
}

void find_in_page_cb(GtkWidget *widget, gpointer *data) {
  GtkHTMLSearchDialog *dialog;
  
  dialog = gtk_html_search_dialog_new(html, TRUE);
  gtk_widget_show_all(GTK_WIDGET(dialog->dialog));
}

void find_again_cb(GtkWidget *widget, gpointer *data) {
  search_next(html);
}
