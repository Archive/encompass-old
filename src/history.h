#ifndef _HISTORY_H_
#define _HISTORY_H_

typedef struct {
  gchar *date;
  gchar *domain;
  gchar *url;
  gchar *title;
} history_item;

#endif
