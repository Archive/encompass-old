#ifndef _TOOLBAR_H_
#define _TOOLBAR_H_

void gnome_snd_event(gchar *snd_event);
void set_fullscreen_tb_mode(gint mode);
void create_toolbars (GtkWidget *app);

#endif
