#include "config.h"

void print_preview_cb (GtkWidget *widget, gpointer data)
{
  GnomePrintMaster *print_master;
  GnomePrintContext *print_context;
  GnomePrintMasterPreview *preview;
  
  if(eprefs.FullScreenMode) {
    return;
  } else {
    print_master = gnome_print_master_new ();
    print_context = gnome_print_master_get_context (print_master);

    gtk_html_print (html, print_context);
    gnome_print_master_close(print_master);

    preview = gnome_print_master_preview_new(print_master, _("Encompass Print Preview"));
    gtk_window_set_wmclass(GTK_WINDOW(preview), "print-preview", "Encompass:Print Preview");
    gtk_widget_show(GTK_WIDGET(preview));
    gtk_object_unref(GTK_OBJECT(print_master));
  }
}

void print_cb(GtkWidget *widget, gpointer data) {
  GnomePrintMaster *print_master;
  GnomePrintContext *print_context;
  gint result;
  
  print_master = gnome_print_master_new ();
  print_context = gnome_print_master_get_context (print_master);
  
  gtk_html_print (html, print_context);
  gnome_print_master_close (print_master);
  
  result =  gnome_print_master_print (print_master);
  if (result == -1) 
     g_print("Printing failed");
  gtk_object_unref(GTK_OBJECT(print_master));
}
