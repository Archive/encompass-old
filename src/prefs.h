#ifndef _PREFS_H_
#define _PREFS_H_

typedef struct _encomprefs encomprefs;
typedef struct _ewidgets ewidgets;

struct _encomprefs {
  gchar *home_url;
  gchar *search_url;
  gint SoundEnabled;
  gint ShowGoButton;
  gint ShowURLBar;
  gint FullScreenMode;
};

struct _ewidgets {
  GtkWidget *home_url;
  GtkWidget *search_url;
};

void pref_dlg_cb(GtkWidget *widget, gpointer data);

#endif
