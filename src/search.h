#ifndef _SEARCH_H_
#define _SEARCH_H_

typedef struct _GtkHTMLSearchDialog  GtkHTMLSearchDialog;

GtkHTMLSearchDialog * gtk_html_search_dialog_new     (GtkHTML *html, gboolean regular);
void                  gtk_html_search_dialog_destroy (GtkHTMLSearchDialog *d);

void                  search                         (GtkHTML *html, gboolean regular);
void                  search_next                    (GtkHTML *html);
void find_in_page_cb(GtkWidget *widget, gpointer *data);
void find_again_cb(GtkWidget *widget, gpointer *data);

#endif
