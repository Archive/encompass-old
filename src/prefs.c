#include "config.h"

static GtkWidget *propbox = NULL;

static void text_changed_cb(GtkWidget *widget, gpointer data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void apply_cb(GtkWidget *w, gint pagenum, gpointer data)
{
  eprefs.home_url = g_strdup(gtk_entry_get_text(GTK_ENTRY(prefwidgets.home_url)));
  if ((pagenum == 0) || (pagenum == -1)) {
    gnome_config_push_prefix("Encompass/");
    gnome_config_set_string("Options/HomeURL", eprefs.home_url);
    gnome_config_sync();
    gnome_config_pop_prefix();
  }
}

static void destroy_cb(GtkWidget *w, gpointer data)
{
  gtk_widget_destroy(propbox);
  propbox = NULL;
}

void pref_dlg_cb(GtkWidget *widget, gpointer data) {
  GtkWidget *cpage;
  GtkWidget *hbox;
  GtkWidget *label, *entry;
  
  if(propbox) return;
  
  propbox = gnome_property_box_new();
  gtk_window_set_modal(GTK_WINDOW(propbox), TRUE);
  gtk_window_set_title(GTK_WINDOW(&GNOME_PROPERTY_BOX(propbox)->dialog.window),
  	_("Encompass Preferences"));
   
  cpage = gtk_vbox_new(FALSE,0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_BIG);

    hbox = gtk_hbox_new(FALSE, 0);
      gtk_widget_show(hbox);
      gtk_container_add(GTK_CONTAINER(cpage), hbox);
    label = gtk_label_new(_("Home Page:"));
      gtk_widget_show(label);
      gtk_container_add(GTK_CONTAINER(hbox), label);
    entry = prefwidgets.home_url = gtk_entry_new();
      gtk_entry_set_text(GTK_ENTRY(entry), eprefs.home_url);
      gtk_signal_connect(GTK_OBJECT(entry), "key_press_event",
		     (GtkSignalFunc) text_changed_cb, NULL);
      gtk_widget_show(entry);
      gtk_container_add(GTK_CONTAINER(hbox), entry);
    
  gtk_widget_show(cpage);
  label = gtk_label_new(_("General"));
  gtk_notebook_append_page(GTK_NOTEBOOK(GNOME_PROPERTY_BOX(propbox)->notebook), cpage, label);

  cpage = gtk_vbox_new(FALSE,0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_BIG);
    
  gtk_widget_show(cpage);
  label = gtk_label_new(_("Connection"));
  gtk_notebook_append_page(GTK_NOTEBOOK(GNOME_PROPERTY_BOX(propbox)->notebook), cpage, label);

  gtk_signal_connect(GTK_OBJECT(propbox), "destroy",
  	GTK_SIGNAL_FUNC(destroy_cb), NULL);
  gtk_signal_connect(GTK_OBJECT(propbox), "apply",
  	GTK_SIGNAL_FUNC(apply_cb), NULL);
  gtk_widget_show(propbox);
}
