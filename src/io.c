/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* io.c
 *
 * Authors: Iain Holmes <iain@helixcode.com>
 *
 * Copyright (C) 2000  Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gnome.h>

#include "io.h"
#include "io-jpg.h"
#include "io-png.h"

gboolean
save_pixbuf (GdkPixbuf *pixbuf,
	     const char *filename)
{
	char *ext;
	
	ext = strrchr (filename, '.');
	if (ext == NULL)
		return FALSE;
	
	/* Probably do this with plugins eventually */
	if ((strcasecmp (ext, ".jpg") == 0)
		|| (strcasecmp (ext, ".jpeg") == 0)) {
		return save_jpeg (pixbuf, filename);
	} else if (strcasecmp (ext, ".png") == 0) {
		return save_png (pixbuf, filename);
	} else {
		g_warning ("Unknown extension %s", ext);
	}
	
	return FALSE;
}

static void
save_file_cb (GtkWidget *button,
	      GdkPixbuf *pixbuf)
{
	GtkFileSelection *filesel;
	char *filename;

	filesel = GTK_FILE_SELECTION (gtk_widget_get_toplevel (button));
	filename = g_strdup (gtk_file_selection_get_filename (filesel));
	gtk_window_set_wmclass(GTK_WINDOW(filesel), "enc-image-save",
			       "Encompass: Save Image");

	if (filename == NULL || *filename == '\0') {
		g_free (filename);
		gtk_widget_destroy (GTK_WIDGET (filesel));
		gdk_pixbuf_unref (pixbuf);
	}
		
	gtk_widget_set_sensitive (GTK_WIDGET (filesel), FALSE);
	while (gtk_events_pending ())
		gtk_main_iteration ();

	/* Do save */
	if (save_pixbuf (pixbuf, filename)) {
		/* No error - Destroy fileselector */
		gtk_widget_destroy (GTK_WIDGET (filesel));
		gdk_pixbuf_unref (pixbuf);
	} else {
		/* Error go round again... */
		g_warning ("Error saving %s\n", filename);
		gtk_widget_set_sensitive (GTK_WIDGET (filesel), TRUE);
	}

	
	g_free (filename);
}

static void
cancel_save_cb (GtkWidget *button,
		GdkPixbuf *pixbuf)
{
	GtkWidget *filesel;

	filesel = gtk_widget_get_toplevel (button);
	gtk_widget_destroy (filesel);
}

void
save_pixbuf_as (GdkPixbuf *pixbuf,
		const char *suggested_filename)
{
	GtkWidget *filesel;

	filesel = gtk_file_selection_new (_("Save image as . . ."));
	gtk_window_set_wmclass(GTK_WINDOW(filesel), "savefile", "Encompass:SaveFile");
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel), 
					 suggested_filename);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button), 
			    "clicked", GTK_SIGNAL_FUNC (save_file_cb), pixbuf);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
			    "clicked", GTK_SIGNAL_FUNC (cancel_save_cb), NULL);
	gtk_widget_show_all (filesel);
}

