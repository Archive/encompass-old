#include "config.h"
#include <gal/widgets/gtk-combo-box.h>

static GtkWidget *go_button, *urlbar;

void gnome_snd_event(gchar *snd_event) {
  if(eprefs.SoundEnabled) {
    gnome_triggers_do(NULL, NULL, "encompass", snd_event, NULL);
  }
}

static void urlbar_hide_cb(GtkWidget *widget, gpointer data)
{
  eprefs.ShowURLBar = GTK_CHECK_MENU_ITEM(widget)->active;
  if(eprefs.ShowURLBar) {
    gtk_widget_realize(urlbar);
    gtk_widget_show(urlbar);
  } else {
    gtk_widget_hide(urlbar);
    gtk_widget_unrealize(urlbar);
  }
  gtk_widget_queue_resize(GTK_WIDGET(apptents));
}

static void go_button_cb(GtkWidget *widget, gpointer data)
{
  eprefs.ShowGoButton = GTK_CHECK_MENU_ITEM(widget)->active;
  if(eprefs.ShowGoButton) {
    gtk_widget_realize(go_button);
    gtk_widget_show(go_button);
  } else {
    gtk_widget_hide(go_button);
    gtk_widget_unrealize(go_button);
  }
  gtk_widget_queue_resize(GTK_WIDGET(urlbar));
}

static GnomeUIInfo buttons_menu[] = {
  GNOMEUIINFO_TOGGLEITEM(N_("_URL Bar"), N_("Show/Hide URL Bar"), urlbar_hide_cb, NULL),
  GNOMEUIINFO_TOGGLEITEM(N_("_Links"), N_("Show/Hide Links toolbar"), NULL, NULL),
  GNOMEUIINFO_TOGGLEITEM(N_("_Radio"), N_("Show/Hide Radio bar"), NULL, NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_TOGGLEITEM(N_("Full Screen"), N_("Full Screen Mode"), full_screen_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo urlbar_menu[] = {
  GNOMEUIINFO_TOGGLEITEM(N_("_URL Bar"), N_("Show/Hide URL Bar"), NULL, NULL),
  GNOMEUIINFO_TOGGLEITEM(N_("_Links"), N_("Show/Hide Links toolbar"), NULL, NULL),
  GNOMEUIINFO_TOGGLEITEM(N_("_Radio"), N_("Show/Hide Radio bar"), NULL, NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_TOGGLEITEM(N_("_Bookmarks"), N_("Show/Hide Bookmarks Quick-File"), NULL, NULL),
  GNOMEUIINFO_TOGGLEITEM(N_("_Go Button"), N_("Show/Hide Go Button"), go_button_cb, NULL),
  GNOMEUIINFO_TOGGLEITEM(N_("Full Screen"), N_("Full Screen Mode"), full_screen_cb, NULL),
  GNOMEUIINFO_END
};

void set_fullscreen_tb_mode(gint mode) {
  GTK_CHECK_MENU_ITEM(urlbar_menu[6].widget)->active = mode;
  GTK_CHECK_MENU_ITEM(buttons_menu[4].widget)->active = mode;
}

void create_toolbars (GtkWidget *app)
{
	GtkWidget *dock;
	GtkWidget *hbox, *hbox2;
	GtkWidget *frame;
	GtkWidget *button, *lnkpix;
	GtkWidget *toolbar;
	GtkWidget *url_pop, *buttons_pop;

	char *imgloc, *lnkfile;

	dock = gnome_dock_item_new ("main-toolbar",
				    (GNOME_DOCK_ITEM_BEH_EXCLUSIVE));
	buttons_pop = gnome_popup_menu_new(buttons_menu);
	gnome_popup_menu_attach(buttons_pop, dock, NULL);
	gnome_app_install_menu_hints(GNOME_APP(app), buttons_menu);
	hbox = gtk_hbox_new (FALSE, 0);
	  gtk_widget_show(hbox);
	gtk_container_add (GTK_CONTAINER (dock), hbox);
	gtk_container_border_width (GTK_CONTAINER (dock), 2);
	
	toolbar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
				   GTK_TOOLBAR_BOTH);
	gtk_widget_show(toolbar);
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar),
			       GTK_TOOLBAR_BOTH);
	gtk_toolbar_set_space_style (GTK_TOOLBAR (toolbar),
				     GTK_TOOLBAR_SPACE_LINE);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar),
				       GTK_RELIEF_NONE);
	gtk_box_pack_start (GTK_BOX (hbox), toolbar, FALSE, FALSE, 0);

	button = gtk_button_new();
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   GTK_SIGNAL_FUNC(back_cb), NULL);
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);

	gtk_container_add(GTK_CONTAINER(button),
			  gnome_stock_new_with_icon(GNOME_STOCK_PIXMAP_BACK));
	gtk_widget_show(button);
	toolbar_back = gtk_combo_box_new(button,
					 gtk_label_new(_("Nothing Yet.")));
	gtk_combo_box_set_arrow_sensitive(GTK_COMBO_BOX(toolbar_back), TRUE);
	gtk_combo_box_set_arrow_relief(GTK_COMBO_BOX(toolbar_back),
				       GTK_RELIEF_NONE);
	gtk_toolbar_append_element(GTK_TOOLBAR(toolbar),
				   GTK_TOOLBAR_CHILD_WIDGET,
				   toolbar_back,
				   _("Back"),
				   _("Go Back"),
				   "",
				   NULL, NULL, NULL);
	gtk_widget_show(toolbar_back);

	gtk_widget_set_sensitive(toolbar_back, FALSE);

	button = gtk_button_new();
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   GTK_SIGNAL_FUNC(forward_cb), NULL);
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_container_add(GTK_CONTAINER(button),
			  gnome_stock_new_with_icon(GNOME_STOCK_PIXMAP_FORWARD));
	gtk_widget_show(button);
	toolbar_forward = gtk_combo_box_new(button,
					    gtk_label_new(_("Nothing Yet.")));
	gtk_combo_box_set_arrow_relief(GTK_COMBO_BOX(toolbar_forward),
				       GTK_RELIEF_NONE);
	gtk_toolbar_append_element(GTK_TOOLBAR(toolbar),
				   GTK_TOOLBAR_CHILD_WIDGET,
				   toolbar_forward,
				   _("Forward"),
				   _("Go Forward"),
				   "",
				   NULL, NULL, NULL);
	gtk_widget_show(toolbar_forward);

	gtk_widget_set_sensitive(toolbar_forward, FALSE);

	gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

	stop_tb = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar),
				 NULL,
				 _("Stop loading"),
				 _("Stop"),
				 gnome_stock_new_with_icon (GNOME_STOCK_PIXMAP_STOP),
				 stop_cb,
				 NULL);
	  gtk_widget_show(stop_tb);
	reload_tb = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar),
				 NULL,
				 _("Reload page"),
				 _("Reload"),
				 gnome_stock_new_with_icon (GNOME_STOCK_PIXMAP_REFRESH),
				 reload_cb, NULL);
	  gtk_widget_show(reload_tb);

	gtk_toolbar_append_item (GTK_TOOLBAR (toolbar),
				 NULL,
				 _("Home page"),
				 _("Home"),
				 gnome_stock_new_with_icon (GNOME_STOCK_PIXMAP_HOME),
				 home_cb, NULL);
	gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
	gtk_toolbar_append_item(GTK_TOOLBAR(toolbar),
		NULL, _("Print the Current Page"), _("Print"),
		gnome_stock_new_with_icon(GNOME_STOCK_PIXMAP_PRINT), print_cb, NULL);

	animator = gnome_animator_new_with_size (32, 32);

	imgloc = gnome_pixmap_file("encompass/32.png");
	gnome_animator_append_frames_from_file_at_size (GNOME_ANIMATOR (animator),
							imgloc,
							0, 0,
							25,
							32, 
							32, 32);

	frame = gtk_frame_new (NULL);
	gtk_container_add (GTK_CONTAINER (frame), animator);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
	gtk_box_pack_end (GTK_BOX (hbox), frame, FALSE, FALSE, 0);
	gnome_animator_set_loop_type (GNOME_ANIMATOR (animator), 
				      GNOME_ANIMATOR_LOOP_RESTART);
	  gtk_widget_show(frame);
	  gtk_widget_show(animator);
	  gtk_widget_show(dock);
	gnome_dock_add_item (GNOME_DOCK (GNOME_APP (app)->dock),
			     GNOME_DOCK_ITEM (dock), GNOME_DOCK_TOP, 1, 0, 0, FALSE);

	/* Create the location bar */
	urlbar = dock = gnome_dock_item_new ("main-urlbar",
				    (GNOME_DOCK_ITEM_BEH_EXCLUSIVE));
 
	url_pop = gnome_popup_menu_new(urlbar_menu);
	gnome_app_install_menu_hints(GNOME_APP(app), urlbar_menu);
	gnome_popup_menu_attach(url_pop, dock, NULL);
	hbox = gtk_hbox_new (FALSE, 2);
	gtk_container_add (GTK_CONTAINER (dock), hbox);
	gtk_container_border_width (GTK_CONTAINER (dock), 2);


	  lnkfile = gnome_pixmap_file("encompass/url.png");
	lnkpix = gnome_pixmap_new_from_file_at_size(lnkfile, 24, 24);
	  gtk_box_pack_start(GTK_BOX(hbox), lnkpix, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (hbox),
			    gtk_label_new (_("Location:")), FALSE, FALSE, 0);
	entry = gtk_combo_new ();
	gtk_combo_disable_activate(GTK_COMBO(entry));
	gtk_signal_connect(GTK_OBJECT(GTK_COMBO(entry)->entry), "activate",
		GTK_SIGNAL_FUNC(entry_goto_url), NULL);
	gtk_signal_connect_after(GTK_OBJECT(GTK_COMBO(entry)->popwin), "button_press_event",
		GTK_SIGNAL_FUNC(entry_goto_selected_url), NULL);
	gtk_box_pack_start (GTK_BOX (hbox),
			    entry, TRUE, TRUE, 0);
	go_button = hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox2), gtk_label_new (_("Go")), FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox2), gnome_stock_new_with_icon (GNOME_STOCK_PIXMAP_REDO), FALSE, FALSE, 0);
	button = gtk_button_new ();
	GTK_WIDGET_UNSET_FLAGS (button, GTK_CAN_FOCUS);
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (button), hbox2);
	gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gnome_dock_add_item (GNOME_DOCK (GNOME_APP (app)->dock),
			     GNOME_DOCK_ITEM (dock), GNOME_DOCK_TOP, 2, 0, 0, FALSE);

}
