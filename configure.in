dnl Process this file with autoconf to produce a configure script.

AC_INIT(configure.in)
AM_INIT_AUTOMAKE(encompass, 0.4.4.1)
AM_CONFIG_HEADER(config.h)

dnl Pick up the Gnome macros.
AM_ACLOCAL_INCLUDE(macros)

GNOME_INIT
AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_STDC
AC_HEADER_STDC

GNOME_COMPILE_WARNINGS
GNOME_X_CHECKS

dnl Add the languages which your application supports here.
ALL_LINGUAS="de fr hu it ja lt nn no ru sv tr"
AM_GNU_GETTEXT

dnl ******************************
dnl GnomePrint checking
dnl ******************************
AC_MSG_CHECKING(for GnomePrint libraries >= 0.24)
if gnome-config --libs print > /dev/null 2>&1; then 
    vers=`gnome-config --modversion print | sed -e "s/gnome-print-//" -e 's/cvs$//' -e 's/pre$//' | \
        awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
    if test "$vers" -ge 24; then
        AC_MSG_RESULT(found)
    else
        AC_MSG_ERROR(You need at least GNOME print 0.24 for this version of Encompass)
    fi
else
    AC_MSG_ERROR(Did not find GnomePrint installed)
fi

dnl ******************************
dnl neon checking
dnl ******************************
AC_MSG_CHECKING(for neon libraries >= 0.15.0)
if neon-config --libs  > /dev/null 2>&1; then 
    vers=`neon-config --version | sed -e "s/neon //" -e 's/cvs$//' -e 's/pre$//' | \
        awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
    if test "$vers" -ge 15; then
        AC_MSG_RESULT(found)
    else
	AC_MSG_RESULT(found "$vers")
        AC_MSG_ERROR(You need at least neon 0.15.0 for this version of Encompass)
    fi
else
    AC_MSG_ERROR(Did not find neon installed)
fi

dnl ******************************
dnl gal checking
dnl ******************************
AC_MSG_CHECKING(for gal libraries >= 0.7)
if gnome-config --libs gal > /dev/null 2>&1; then 
    vers=`gnome-config --modversion gal | sed -e "s/gal-//" -e 's/cvs$//' -e 's/pre$//' | \
        awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
    if test "$vers" -ge 7; then
        AC_MSG_RESULT(found)
    else
	AC_MSG_RESULT(found "$vers")
        AC_MSG_ERROR(You need at least gal 0.7 for this version of Encompass)
    fi
else
    AC_MSG_ERROR(Did not find gal installed)
fi

dnl ******************************
dnl libxml checking
dnl ******************************
AC_MSG_CHECKING(for libxml  >= 1.8.12)
if gnome-config --libs xml > /dev/null 2>&1; then 
    vers=`gnome-config --modversion xml | sed -e "s/xml-//" -e 's/cvs$//' -e 's/pre$//' | \
        awk 'BEGIN { FS = "."; } { print $3;}'`
    if test "$vers" -ge 12; then
        AC_MSG_RESULT([found ("$vers")])
    else
	AC_MSG_RESULT([found ("$vers")])
        AC_MSG_ERROR(You need at least libxml 1.8.12 for this version of Encompass)
    fi
else
    AC_MSG_ERROR(Did not find libxml installed)
fi

dnl gdk-pixbuf checks.

AC_MSG_CHECKING([for gdk-pixbuf >= 0.8.0])

if gnome-config --libs gdk_pixbuf > /dev/null 2>&1; then
	vers=`gnome-config --modversion gdk_pixbuf | sed 's/^gdk-pixbuf-//'`
        major=`echo "$vers" | sed 's/\([[0-9]]*\)\.[[0-9]]*\.[[0-9]]*/\1/'`
        minor=`echo "$vers" | sed 's/[[0-9]]*\.\([[0-9]]*\)\.[[0-9]]*/\1/'`

	if [[ "x$major" = "x" ]] ; then
		gdkpixbuf_ok=false
        elif [[ "x$major" = "x0" ]] ; then
		case "$minor" in
			[[01234567]])
				gdkpixbuf_ok=false ;;
                        *)
				gdkpixbuf_ok=true ;;
		esac
	else
		gdkpixbuf_ok=true
	fi
	AC_MSG_RESULT([found ("$vers")])
else
	gdkpixbuf_ok=false
	AC_MSG_RESULT([not found])
fi

if test x$gdkpixbuf_ok = xfalse ; then
	AC_MSG_ERROR([gdk-pixbuf 0.8.0 or later is required to compile Encompass.])
fi

dnl Setup extra GNOME library flags.

EXTRA_GNOME_LIBS=`gnome-config --libs gnomeui gdk_pixbuf print unicode gtkhtml vfs bonobo oaf`
EXTRA_GNOME_CFLAGS=`gnome-config --cflags gnomeui gdk_pixbuf print unicode gtkhtml vfs bonobo oaf`

AC_SUBST(EXTRA_GNOME_LIBS)
AC_SUBST(EXTRA_GNOME_CFLAGS)

dnl Neon Libraries.

NEON_CFLAGS=`neon-config --cflags`
NEON_LIBS=`neon-config --libs`

AC_SUBST(NEON_CFLAGS)
AC_SUBST(NEON_LIBS)

dnl Set PACKAGE_LOCALE_DIR in config.h.
if test "x${prefix}" = "xNONE"; then
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${ac_default_prefix}/${DATADIRNAME}/locale")
else
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${prefix}/${DATADIRNAME}/locale")
fi

AC_MSG_CHECKING([whether to install into Gnome's prefix])
if test x"$use_system_install" = xyes ; then
	AC_MSG_RESULT([yes])
	gnomedatadir=`${GNOME_CONFIG} --datadir`
	gnomeconfdir=`${GNOME_CONFIG} --sysconfdir`
else
	AC_MSG_RESULT([no])
	gnomedatadir="${datadir}"
	gnomeconfdir="${sysconfdir}"
fi

AC_SUBST(gnomedatadir)
AC_SUBST(gnomeconfdir)

GNOME_HELP_DIR="`gnome-config --datadir`/gnome/help"
AC_SUBST(GNOME_HELP_DIR)

dnl Subst PACKAGE_PIXMAPS_DIR.
PACKAGE_PIXMAPS_DIR="`gnome-config --datadir`/pixmaps/${PACKAGE}"
AC_SUBST(PACKAGE_PIXMAPS_DIR)

AC_OUTPUT([
Makefile
encompass.spec
intl/Makefile
macros/Makefile
src/Makefile
pixmaps/Makefile
sounds/Makefile
po/Makefile.in
])

